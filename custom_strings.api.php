<?php

/**
 * @file
 * Hooks provided by Custom strings.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Collects a list of contexts
 *
 * @return array $contexts
 *   A list of contexts used by the custom strings module
 *
 * @see \Drupal\views\Plugin\DsPluginManager
 */
function hook_custom_strings_info() {
  return array(
    'foo' => 'Bar',
  );
}

/**
 * @} End of "addtogroup hooks".
 */
