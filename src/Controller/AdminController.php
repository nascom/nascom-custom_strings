<?php

namespace Drupal\custom_strings\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Display Suite UI routes.
 */
class AdminController extends ControllerBase {

  /**
   * Lists all translations
   *
   * @return array
   *   The overview page
   */
  public function displayList() {
    return array(
      'filter' => $this->formBuilder()->getForm('Drupal\custom_strings\Form\TranslateFilterForm'),
      'form' => $this->formBuilder()->getForm('Drupal\custom_strings\Form\TranslateEditForm'),
    );
  }

}
