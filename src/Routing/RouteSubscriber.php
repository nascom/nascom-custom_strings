<?php

namespace Drupal\custom_strings\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('locale.translate_page')) {
      $route->setDefault('_controller', '\Drupal\custom_strings\Controller\AdminController::displayList');
    }
  }

}
